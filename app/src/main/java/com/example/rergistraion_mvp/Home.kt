package com.example.rergistraion_mvp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.rergistraion_mvp.ui.login.MainActivity
import com.example.rergistraion_mvp.ui.productlist.Productlist
import com.example.rergistraion_mvp.util.SharedPrefrenceManager
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.navigation_header.view.*


class Home : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

    val toolbar = findViewById<Toolbar>(R.id.toolbar)
    toolbar.title = ""
    setSupportActionBar(toolbar)
    val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
    val toggle = ActionBarDrawerToggle(this,
            drawer,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
    drawer.setDrawerListener(toggle)
    toggle.syncState()
    val navigationView = findViewById<NavigationView>(R.id.nav_view)
    val headerView = navigationView.getHeaderView(0)
    headerView.txtFullName.setText(SharedPrefrenceManager.getInstance(applicationContext).user.email_id)
        productCard.setOnClickListener {
            val intent = Intent(this, Productlist::class.java)
            startActivity(intent)
        }
}

override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.home, menu)
    return true
}

override fun onOptionsItemSelected(item: MenuItem): Boolean {
    if (item.itemId == R.id.menu_logout){
        finish()
        SharedPrefrenceManager.getInstance(applicationContext).clear()
        startActivity(Intent(applicationContext, MainActivity::class.java))
    }
    return super.onOptionsItemSelected(item)
}
override fun onBackPressed() {
    val drawer =
        findViewById<View>(R.id.drawer_layout) as DrawerLayout
    if (drawer.isDrawerOpen(GravityCompat.START)) {
        drawer.closeDrawer(GravityCompat.START)
    } else {
        super.onBackPressed()
    }
}

override fun onStart() {
    super.onStart()

    if(!SharedPrefrenceManager.getInstance(this).isLoggedIn){
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }
}
}

