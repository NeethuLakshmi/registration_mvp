package com.example.rergistraion_mvp.model.register

import com.example.rergistraion_mvp.model.login.User

data class SignupResponse(val status: String, val message:String, val data: User)