package com.example.rergistraion_mvp.model.register

data class CityResponse ( val data: List<CityData>,
                     val message: String,
                     val status: String)