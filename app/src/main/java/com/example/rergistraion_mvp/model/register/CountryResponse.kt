package com.example.rergistraion_mvp.model.register

data class CountryResponse (  val data: List<CountryData>,
                              val message: String,
                              val status: String)