package com.example.rergistraion_mvp.model.register

data class CityData (val city_id: String,
                     val city_name: String)