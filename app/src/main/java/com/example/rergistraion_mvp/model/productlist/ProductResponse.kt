package com.example.rergistraion_mvp.model.productlist

data class ProductResponse (val data: List<ProductData>,
                            val message: String,
                            val status: String)