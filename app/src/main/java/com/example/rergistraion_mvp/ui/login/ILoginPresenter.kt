package com.example.rergistraion_mvp.ui.login

interface ILoginPresenter{
    fun callLoginAPI(email: String, password: String, userType: Int, providerType: Int)
}