package com.example.rergistraion_mvp.ui.productlist;

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.rergistraion_mvp.R
import com.example.rergistraion_mvp.model.productlist.ProductData;
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.app_bar_productlist.view.*

class ProAdapter(private val dataList: MutableList<ProductData>) : RecyclerView.Adapter<Myholder>() {

        private lateinit var context: Context

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Myholder {
                context = parent.context
                return Myholder(
                        LayoutInflater.from(
                                context
                        ).inflate(
                                R.layout.app_bar_productlist,
                                parent,
                                false
                        )
                )
        }

        override fun getItemCount(): Int = dataList.size

        override fun onBindViewHolder(holder: Myholder, position: Int) {
                val data = dataList[position]

                val productName = holder.itemView.txtproduct_title_id
                val productImage = holder.itemView.product_img
                val productPrice = holder.itemView.txtproduct_price_id

                val ItemName = data.product_name
                productName.text = ItemName
                val ItemPrice = data.price
                productPrice.text = "₹" + ItemPrice

                Picasso.get()
                        .load(data.image_url)
                        .into(productImage)

                holder.itemView.setOnClickListener {
                        Toast.makeText(context, ItemName, Toast.LENGTH_SHORT).show()
                }
        }
}

