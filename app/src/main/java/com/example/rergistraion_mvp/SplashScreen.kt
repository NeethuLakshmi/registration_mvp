package com.example.rergistraion_mvp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.rergistraion_mvp.ui.login.MainActivity
import com.example.rergistraion_mvp.util.SharedPrefrenceManager

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (SharedPrefrenceManager.getInstance(this).isLoggedIn) {
            val intent = Intent(applicationContext, Home::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        } else {
            startActivity(Intent(applicationContext, MainActivity::class.java))
            finish()
        }


    }
}